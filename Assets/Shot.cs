﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour
{
    [SerializeField] private GameObject fromObject;
    [SerializeField] private GameObject prefabPulya;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(fromObject.transform.position, fromObject.transform.forward, out RaycastHit hit))
            {
                Instantiate(prefabPulya, hit.point, Quaternion.identity, hit.collider.transform);

                TargetHandler obj = hit.collider.gameObject.GetComponent<TargetHandler>();
                if (obj != null)
                {
                    obj.ShotReceive();
                }
            }
        }
    }
}
