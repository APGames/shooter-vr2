﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEyes : MonoBehaviour
{
    private EnemyMover parent;

    private void Start()
    {
        parent = transform.parent.gameObject.GetComponent<EnemyMover>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            parent.targetPlayer = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == parent.targetPlayer)
        {
            parent.vertical = 0;
            parent.horizontal = 0;
            parent.targetPlayer = null;
        }
    }
}
