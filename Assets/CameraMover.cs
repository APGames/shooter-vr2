﻿using UnityEngine;

public class CameraMover : MonoBehaviour
{
    [SerializeField] private Transform player;

    [Header("Game Settings")]

    [Range(0.5f, 3)]
    [SerializeField] private float sensivity;

    private float angleX;

    private void Start()
    {
        angleX = 0;
    }

    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * sensivity;
        float mouseY = Input.GetAxis("Mouse Y") * sensivity;

        angleX -= mouseY;
        angleX = Mathf.Clamp(angleX, -60, 60);
        player.Rotate(new Vector3(0, mouseX, 0));
        transform.localRotation = Quaternion.Euler(angleX, 0, 0);
    }
}
